/*!
 * Module dependencies.
 */

const { dbFeedService } = require('../services');

exports.index = async (_req, res) => {
  res.render('home/index', {
    title: 'Node Express Mongoose Boilerplate',
  });
};

exports.initialData = async (_req, res, next) => {
  try {
    await dbFeedService.insertInitialFigures();
    await dbFeedService.addRule();
    res.status(201).json({ success: true });
  } catch (err) {
    next(new Error('error while inserting figures'));
  }
};
