const orderController = require('./order.controller');

describe('Order controller', () => {
  let mockRequest;
  let mockResponse;
  let nextFunction;

  beforeEach(() => {
    const req = {};
    mockRequest = {
      body: jest.fn().mockReturnValue(req),
      params: jest.fn().mockReturnValue(req),
    };
    mockResponse = {
      status: jest.fn().mockReturnThis(), // This line
      send: jest.fn(), // also mocking for send function
      json: jest.fn().mockReturnThis(),
    };
    nextFunction = jest.fn().mockReturnThis();
  });

  test('should call next function as body is null', async () => {
    let req = mockRequest;
    req.body.customer = null;

    await orderController.newOrder(req, mockResponse, nextFunction);

    expect(nextFunction).toHaveBeenCalledTimes(1);
    // expect(mockResponse).toHaveBeenCalledTimes(1);
    // expect(mockResponse.json).toHaveBeenCalledWith({
    //   success: false,
    //   message: 'missing data',
    // });
  });
});
