/*!
 * Module dependencies.
 */

const { userService, orderService, factoryService } = require('../services');

/**
 * newOrder controller
 *
 * @param {Express.Request} _req
 * @param {Express.Response} res
 * @param {Express.NextFunction} next
 * @return {Void}
 */
exports.newOrder = async (req, res, next) => {
  console.log('app.controller.orderController.newOrder');
  const { customer, shippingAdress, items } = req.body;
  if (!customer || !shippingAdress || !items) {
    return next(new Error('missing data'));
  }
  console.log({ customer, shippingAdress, items });
  try {
    const customerId = await userService.newUser({ ...customer });
    const newOrderId = await orderService.newOrder({
      customerId,
      shippingAdress,
      items,
    });
    const total = await orderService.getTotal(newOrderId);
    // update total in the db
    await orderService.updateOrder(newOrderId, { total });
    await factoryService.newProductionRequest(newOrderId);
    // send to production

    console.log('app.controller.orderController.newOrder.succeed');
    res.status(201).json({ success: true, newOrderId, total });
  } catch (err) {
    console.log('app.controller.orderController.newOrder.error');
    next(err);
  }
};
