const orderController = require('./order.controller');
const homeController = require('./home.controller');

module.exports = {
  orderController,
  homeController,
};
