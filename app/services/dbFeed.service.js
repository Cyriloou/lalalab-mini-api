const { FigureModel } = require('../models/figure.model');
const { RuleModel } = require('../models/rule.model');

/**
 * Insert figures in db
 * Use for populate the db
 *
 * @param {String} orderId
 * @return {Mongoose.Document}
 */
exports.insertInitialFigures = () => {
  return FigureModel.insertMany([
    {
      name: 'MiNi ﬁgure',
      price: 15,
    },
    {
      name: 'MiNi Familly pack',
    },
  ]);
};

/**
 * addRule: Insert rules in db
 * not used but could be use later on in order to get discount in db
 *
 * @param {String} orderId
 * @return {Void}
 */
exports.addRule = async () => {
  const figure = await FigureModel.findOne({
    name: 'MiNi ﬁgure',
  });
  const rule = new RuleModel({
    figureId: figure._id,
    rule: `<%= qty > 50 %> `,
    newPrice: 9,
  });
  await rule.save();

  const familly = await FigureModel.findOne({
    name: 'MiNi Familly pack',
  });
  const rule2 = new RuleModel({
    figureId: familly._id,
    rule: `<%= qty >= 1 %> `,
    discountTotal: 20,
  });
  await rule2.save();
};
