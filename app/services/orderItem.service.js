const { FigureModel } = require('../models/figure.model');
const { OrderItemModel } = require('../models/orderItem.model');

/**
 * Update an orderItem in db
 *
 * @param {String | Object} orderItemId
 * @param {Object} payload
 * @return {Mongoose.Document}
 */
exports.updateOrderItem = async (orderItemId, payload) =>
  OrderItemModel.updateOne(
    { _id: orderItemId },
    {
      ...payload,
    },
  );

/**
 * Insert an orderItem in db
 *
 * @param {Array} items
 * @return {Array<Mongoose.ObjectId>}
 */
exports.newOrderItems = async (items) => {
  console.log('app.services.orderItemService.newOrderItems');
  const namesList = items.map((el) => el.name).reduce((acc, name) => (acc.some((el) => el === name) ? acc : [...acc, name]), []);
  console.log({ namesList });
  const figures = await FigureModel.find({
    name: { $in: namesList },
  });
  console.log({ figures });
  const orderItems = await OrderItemModel.insertMany(
    items.map((i) => {
      const figure = figures.find((f) => f.name === i.name);
      console.log({ figure });
      return {
        figureId: figure._id,
        qty: i.qty,
        picture: i.picture,
        // serial: String,
        // prodStatus: String
      };
    }),
  );
  console.log(
    'app.services.orderItemService.newOrderItems',
    orderItems.map((el) => el._id),
  );
  return orderItems.map((el) => el._id);
};
