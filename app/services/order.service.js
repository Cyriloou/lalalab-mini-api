const orderItemService = require('./orderItem.service');
const { OrderModel } = require('../models/order.model');
const { OrderItemModel } = require('../models/orderItem.model');
const { FigureModel } = require('../models/figure.model');
const { UserModel } = require('../models/user.model');

/**
 * Update an order in db
 *
 * @param {String | Object} orderId
 * @param {Object} payload
 * @return {Mongoose.Document}
 */
exports.updateOrder = async (orderId, payload) =>
  OrderModel.updateOne(
    { _id: orderId },
    {
      ...payload,
    },
  );

/**
 * Insert an order in db
 *
 * @param {String | Object} customerId
 * @param {Object} shippingAdress
 * @param {Array} items
 * @return {Mongoose.ObjectId}
 */
exports.newOrder = async ({ customerId, shippingAdress, items }) => {
  console.log('app.services.orderService.newOrder');
  const total = 0;

  const orderItemsIds = await orderItemService.newOrderItems([...items]);
  console.log({ orderItemsIds });
  let newOrder = await OrderModel.create({
    customer: customerId,
    items: orderItemsIds,
    total: total,
    shippingAddress: {
      ...shippingAdress,
    },
  });

  await newOrder.save();
  console.log('app.services.orderService.newOrder', newOrder._id);
  return newOrder._id;
};

/**
 * fetch full order from id
 *
 * @param {Mongoose.ObjectId} id
 * @return {Object}
 */
exports.getFullOrderFromId = async (id) => {
  const order = await OrderModel.findOne({ _id: id }).exec();
  const items = await OrderItemModel.find({ _id: { $in: order.items } });
  const customer = await UserModel.findOne({ _id: order.customer });
  return {
    ...order,
    items,
    customer,
  };
};

/**
 * fetch total amount of an order from id
 *
 * @param {Mongoose.ObjectId} id
 * @return {Object}
 */
exports.getTotal = async (id) => {
  console.log('app.services.orderService.getTotal');
  const order = await this.getFullOrderFromId(id);
  const figures = await FigureModel.find({ _id: { $in: order.items.map((el) => el.figureId) } });

  const totalItemList = figures.map((f) => {
    const qty = order.items.filter((i) => `${i.figureId}` === `${f._id}`).reduce((count, val) => count + val.qty, 0);
    return {
      _id: f._id,
      name: f.name,
      price: f.name === 'MiNi ﬁgure' && qty > 50 ? 9 : f.price,
      qty: qty,
    };
  });

  let total = totalItemList.reduce((count, val) => count + val.price * val.qty, 0);
  const isFamillyPack = totalItemList.some((t) => t.name === 'MiNi Familly pack');
  if (isFamillyPack) {
    total = total * (1 - 20 / 100);
  }
  console.log('app.services.orderService.getTotal', total);
  return total;
};
