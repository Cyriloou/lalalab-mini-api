const { UserModel } = require('../models/user.model');

/**
 * create a new user
 *
 * @param {String} email
 * @param {String} name
 * @param {String} password
 * @return {Mongoose.ObjectId}
 */
exports.newUser = async ({ email, name, password }) => {
  console.log('app.services.userService.newUser');
  console.log({ email, name, password });
  var user = new UserModel({
    name: name,
    email: email,
    hashed_password: password,
  });
  await user.save(); // save into DB
  console.log('app.services.userService.succeed', user._id);
  return user._id;
};
