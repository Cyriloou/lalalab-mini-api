const orderService = require('./order.service');
const { OrderModel, statusEnum } = require('../models/order.model');
const { OrderItemModel } = require('../models/orderItem.model');
const { v4: uuidv4 } = require('uuid');
const orderItemService = require('./orderItem.service');
var CronJob = require('cron').CronJob;

/**
 * Send an order to production
 *
 * @param {String} orderId
 * @return {Void}
 */
exports.newProductionRequest = async (orderId) => {
  console.log('app.services.factoryService.newProductionRequest');
  const order = await orderService.getFullOrderFromId(orderId);
  const orderItemsId = order.items.map((i) => i._id);
  setTimeout(async () => {
    // simultate call to factory
    await orderService.updateOrder(orderId, {
      deliveryStatus: statusEnum.WITH_PRODUCTION,
    });
    await Promise.all(
      orderItemsId.map((id) =>
        OrderItemModel.updateOne(
          { _id: id },
          {
            serial: uuidv4(),
            prodStatus: statusEnum.WITH_PRODUCTION,
          },
        ),
      ),
    );
  }, 1000);
};

/**
 * productionJob: cron to simulate production answers
 *
 * @return {Void}
 */
exports.productionJob = () => {
  new CronJob(
    '*/1 * * * *', // every 1 minutes
    async () => {
      acknoledgeOrders();
    },
    null,
    true,
    'Europe/Paris',
  );
};

/**
 * acknoledgeOrders: function that simulate production message
 * Sent a status and acknoledge order for shipping
 *
 * @return {void}
 */
const acknoledgeOrders = async () => {
  console.log('app.services.factoryService.acknoledgeOrders');
  // get orders with production and update items status
  const ordersWithProduction = await OrderModel.find({
    deliveryStatus: statusEnum.WITH_PRODUCTION,
  });
  console.log({ ordersWithProduction });
  // update items status
  const readyOrders = await Promise.all(
    ordersWithProduction.map((o) => {
      if (getRandomInt(2)) {
        return orderItemService
          .updateOrderItem(
            { $in: o.items },
            {
              deliveryStatus: statusEnum.READY,
            },
          )
          .then(() => o._id);
      }
      return false;
    }),
  );
  console.log({ readyOrders });
  // update orders status
  await Promise.all(
    readyOrders
      .filter((el) => !!el)
      .map((id) => {
        return orderService.updateOrder(
          { $in: id },
          {
            deliveryStatus: statusEnum.READY,
          },
        );
      }),
  );
  // sent out email
  await Promise.all(
    readyOrders
      .filter((el) => !!el)
      .map((id) => {
        const order = ordersWithProduction.find((el) => el._id === id);
        console.log(' ######## ');
        console.log(' ######## ');
        console.log(' ######## ');
        console.log(order._id, ' sent to customer for total invoice ', order.total, '€');
      }),
  );
  console.log('app.services.factoryService.acknoledgeOrders.succeed');
};

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}
