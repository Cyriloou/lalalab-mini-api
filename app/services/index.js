const userService = require('./user.service');
const dbFeedService = require('./dbFeed.service');
const orderService = require('./order.service');
const orderItemService = require('./orderItem.service');
const factoryService = require('./factory.service');

module.exports = {
  userService,
  dbFeedService,
  orderService,
  orderItemService,
  factoryService,
};
