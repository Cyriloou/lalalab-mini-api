/*!
 * Module dependencies
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * OrderItem schema
 */

const OrderItemSchema = new Schema(
  {
    figureId: mongoose.ObjectId,
    qty: Number,
    picture: String,
    serial: String,
    prodStatus: String,
  },
  { timestamps: true },
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

// OrderItemSchema.method({});

/**
 * Statics
 */

// OrderItemSchema.static({});

/**
 * Register
 */

const OrderItemModel = mongoose.model('OrderItem', OrderItemSchema);

module.exports = { OrderItemModel, OrderItemSchema };
