/*!
 * Module dependencies
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const statusEnum = {
  CREATED: 'CREATED',
  WITH_PRODUCTION: 'WITH_PRODUCTION',
  READY: 'READY',
  SENT: 'SENT',
};

/**
 * Order schema
 */

const OrderSchema = new Schema(
  {
    customer: mongoose.ObjectId, // UserSchema,
    items: [mongoose.ObjectId], // [OrderItemSchema],
    total: Number,
    shippingAddress: {
      address: String,
      city: String,
      postcode: String,
      country: String,
      phoneNumber: String,
    },
    deliveryStatus: { type: String, default: statusEnum.CREATED },
  },
  { timestamps: true },
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

// OrderSchema.method({});

/**
 * Statics
 */

// OrderSchema.static({});

/**
 * Register
 */

const OrderModel = mongoose.model('Order', OrderSchema);

module.exports = { OrderModel, OrderSchema, statusEnum };
