/*!
 * Module dependencies
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Rule schema
 */

const RuleSchema = new Schema(
  {
    figureId: mongoose.ObjectId, // RuleSchema,
    rule: String,
    newPrice: Number,
    discountTotal: Number,
  },
  { timestamps: true },
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

// RuleSchema.method({});

/**
 * Statics
 */

// RuleSchema.static({});

/**
 * Register
 */

const RuleModel = mongoose.model('Rule', RuleSchema);

module.exports = { RuleSchema, RuleModel };
