/*!
 * Module dependencies
 */
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * User schema
 */

const UserSchema = new Schema(
  {
    name: { type: String, default: '' },
    email: { type: String, required: true, unique: false },
    hashed_password: { type: String, default: '', required: true },
    salt: { type: String, default: '' },
  },
  { timestamps: true },
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

UserSchema.pre('save', function (next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('hashed_password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.hashed_password, salt, function (err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      user.hashed_password = hash;
      user.salt = SALT_WORK_FACTOR;
      next();
    });
  });
});

/**
 * Methods
 */

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.hashed_password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

/**
 * Statics
 */

// UserSchema.static({});

/**
 * Register
 */

const UserModel = mongoose.model('User', UserSchema);
module.exports = { UserModel, UserSchema };
