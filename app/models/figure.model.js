/*!
 * Module dependencies
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Figure schema
 */

const FigureSchema = new Schema(
  {
    name: { type: String, required: true, unique: true },
    price: { type: Number, default: 0 },
  },
  { timestamps: true },
);

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

// FigureSchema.method({});

/**
 * Statics
 */

// FigureSchema.static({});

/**
 * Register
 */

const FigureModel = mongoose.model('Figure', FigureSchema);

module.exports = { FigureSchema, FigureModel };
