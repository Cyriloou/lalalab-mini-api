/**
 * errorMiddleware
 * get errors and return message
 *
 * @param {Error} error
 * @param {Express.Request} _req
 * @param {Express.Response} res
 * @param {Express.NextFunction} next
 * @return {Void}
 */
module.exports.errorMiddleware = (error, _req, res, next) => {
  console.log('app.middlewares.error.middleware');
  if (res.headersSent) {
    // So when you add a custom error handler, you must delegate to the default Express error handler, when the headers have already been sent to the client:
    next(error);
  } else {
    const status = error.statusCode || 500;
    const message = error?.response?.message || error.message || 'Something went wrong';
    res.status(status).json({
      success: false,
      message: message,
    });
  }
};
