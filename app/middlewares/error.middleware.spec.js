const { errorMiddleware } = require('./error.middleware');

describe('Error handler middleware', () => {
  const error = {
    name: 'error',
    statusCode: 500,
    status: 1,
    message: 'string',
    error: 'string',
  };
  let mockRequest;
  let mockResponse;
  let nextFunction = jest.fn();

  beforeEach(() => {
    mockRequest = {};
    mockResponse = {
      status: jest.fn().mockReturnThis(), // This line
      send: jest.fn(), // also mocking for send function
      json: jest.fn().mockReturnThis(),
      // status: (s) => {
      //   this.statusCode = s;
      //   return this;
      // // },
    };
  });

  test('handle error when error includes statusCode', async () => {
    errorMiddleware(error, mockRequest, mockResponse, nextFunction);

    expect(mockResponse.status).toHaveBeenCalledWith(500);
    expect(mockResponse.json).toHaveBeenCalledWith({
      success: false,
      message: error.message,
    });
    expect(nextFunction).not.toHaveBeenCalled();
  });
});
