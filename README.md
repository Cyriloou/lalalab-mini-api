## start project
### Install
1. node via nvm. version v16.13.2
2. install dependencies with `yarn`
3. add to .env file


PORT=3002

### Run
 `yarn run start`

### Test
 `yarn run test`

### Connect to Postman
 `https://www.getpostman.com/collections/83902ccdfea84a6557d3`

### Fonctionnement
1. lancer l'app
2. ouvrir postman et lancer une insertion `New Order`
3. Dans les logs appraissent l'insertion, la MAJ du status puis les infos d'expédition.

Les retours de l'usine sont fait avec un cron toutes les 1 minute


### notes
Premier dev 
Axes d'amélioration
- Dockeriser
- renforcer la couverture de test
- utiliser passport pour proteger les routes
- ajouter des cors pour proteger l'API
- stocker en db les règles de reductions
- ajouter un nodemailer pour envoyer les MAJ de statut au client
- ajouter un outil PDF pour envoyer la facture
- Création d'un compte utilisateur
