const express = require('express');
const { orderController } = require('../../app/controllers');

const router = express.Router();

router.post(
  '/',
  // policies.isBearerAuthenticated,
  orderController.newOrder,
);

module.exports = router;
