'use strict';

const { homeController } = require('../app/controllers');
/**
 * Module dependencies.
 */

const { errorMiddleware } = require('../app/middlewares/error.middleware');

/**
 * Expose
 */

module.exports = function (app) {
  app.use((req, _res, next) => {
    console.log({
      method: req.method,
      body: req.body,
      query: req.query,
      params: req.params,
      originalUrl: req.originalUrl,
      headers: req.headers,
    });
    next();
  });

  app.get('/', homeController.index);
  app.get('/initialData', homeController.initialData);

  app.use('/orders', require('./routes/order.route'));

  app.use(errorMiddleware);

  /**
   * Error handling
   */

  app.use(function (err, _req, res, next) {
    // treat as 404
    if (err.message && (~err.message.indexOf('not found') || ~err.message.indexOf('Cast to ObjectId failed'))) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).render('500', { error: err.stack });
  });

  // assume 404 since no middleware responded
  app.use(function (req, res) {
    res.status(404).render('404', {
      url: req.originalUrl,
      error: 'Not found',
    });
  });
};
